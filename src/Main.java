public class Main {
    /**
     * Main method runs the calculate() method
     */
    public static void main(String[] args) {
        calculate(10);
        calculate(600);
        calculate(3600);
        calculate(10000);
        calculate(20000);
        calculate(30000);
        betterSolution(10000);
    }

    /**
     * The calculate()-method takes a number of seconds and convert it to the format HH:MM:SS
     * @param input Input seconds to convert
     */
    public static void calculate(int input){
        int hours;
        int minutes;
        int seconds;
        int secondsLeft;

        if(input > 0 && input < 359999)
        {
            // Calcualate
            hours = input / 3600;                       // Calculates the number of hours
            secondsLeft = input - hours * 3600;         // Finds out how many seconds there is left to calculate
            minutes = secondsLeft / 60;                 // Calculates the number of minutes
            secondsLeft = secondsLeft - minutes * 60;   // Find out how many seconds there is left
            seconds = secondsLeft;                      // The seconds left

            String hoursString = "" + hours;            // Converts the values to a String to make it easier
            String minutesString = "" + minutes;        // to make the format correct
            String secondsString = "" + seconds;

            if (hours < 10){
                hoursString = "0" + hours;
            }
            if (minutes < 10){
                minutesString = "0" + minutes;          // Adds a "0" in front if the number is lower than 10
            }                                           // This is to get the format correct
            if (seconds < 10){
                secondsString = "0" + seconds;
            }

            // Print
            System.out.println("Seconds to convert: " + input + " -> " + hoursString + ":" + minutesString + ":" + secondsString);
            System.out.println();
        }
        else {
            System.out.println("Invalid input");
        }
    }

    /**
     * The betterSolution() method takes a number of seconds and convert it to the format HH:MM:SS
     * This solution is neater and better than the calculate()-method
     * @param input Input seconds to convert
     */
    public static void betterSolution(int input){
        int hours = input / 3600;
        int minutes = (input % 3600) / 60;
        int seconds = input % 60;

        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        System.out.println(timeString);
    }
}
